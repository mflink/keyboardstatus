using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace KeyboardStatus {
  public partial class FormSettings : Form {
    public FormSettings() {
      InitializeComponent();

      this.pictureBoxIconsMulti1.Image = Properties.Resources._111_1.ToBitmap();

      this.pictureBoxIconsSingle1_numlock.Image = Properties.Resources.numlock_1.ToBitmap();
      this.pictureBoxIconsSingle1_capslock.Image = Properties.Resources.capslock_1.ToBitmap();
      this.pictureBoxIconsSingle1_scroll.Image = Properties.Resources.scroll_1.ToBitmap();
      this.pictureBoxIconsSingle2_numlock.Image = Properties.Resources.numlock_2.ToBitmap();
      this.pictureBoxIconsSingle2_capslock.Image = Properties.Resources.capslock_2.ToBitmap();
      this.pictureBoxIconsSingle2_scroll.Image = Properties.Resources.scroll_2.ToBitmap();

      this.panelIconsSingle.Enabled = false;
      this.panelIconsMulti.Enabled = true;
    }

    private void radioButtonIconsMulti_CheckedChanged(object sender, EventArgs e) {
      this.panelIconsSingle.Enabled = false;
      this.panelIconsMulti.Enabled = true;

      ((FormStatus)this.Owner).setUsesSingleIcons(this.panelIconsSingle.Enabled);
    }

    private void radioButtonIconsSingle_CheckedChanged(object sender, EventArgs e) {
      this.panelIconsSingle.Enabled = true;
      this.panelIconsMulti.Enabled = false;

      ((FormStatus)this.Owner).setUsesSingleIcons(this.panelIconsSingle.Enabled);
    }
  }
}