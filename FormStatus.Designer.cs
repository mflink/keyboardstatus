﻿namespace KeyboardStatus {
  partial class FormStatus {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent() {
      this.components = new System.ComponentModel.Container();
      this.chkCapsLock = new System.Windows.Forms.CheckBox();
      this.chkNumLock = new System.Windows.Forms.CheckBox();
      this.chkScroll = new System.Windows.Forms.CheckBox();
      this.timer = new System.Windows.Forms.Timer(this.components);
      this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
      this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.menuItemShowDialog = new System.Windows.Forms.ToolStripMenuItem();
      this.menuItemHideDialog = new System.Windows.Forms.ToolStripMenuItem();
      this.menuItemSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.menuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.menuItemSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.menuItemExit = new System.Windows.Forms.ToolStripMenuItem();
      this.notifyIconNumLock = new System.Windows.Forms.NotifyIcon(this.components);
      this.notifyIconCapsLock = new System.Windows.Forms.NotifyIcon(this.components);
      this.notifyIconScroll = new System.Windows.Forms.NotifyIcon(this.components);
      this.contextMenu.SuspendLayout();
      this.SuspendLayout();
      // 
      // chkCapsLock
      // 
      this.chkCapsLock.AutoCheck = false;
      this.chkCapsLock.AutoSize = true;
      this.chkCapsLock.Location = new System.Drawing.Point(12, 35);
      this.chkCapsLock.Name = "chkCapsLock";
      this.chkCapsLock.Size = new System.Drawing.Size(89, 17);
      this.chkCapsLock.TabIndex = 0;
      this.chkCapsLock.Text = "Capital letters";
      this.chkCapsLock.UseVisualStyleBackColor = true;
      // 
      // chkNumLock
      // 
      this.chkNumLock.AutoCheck = false;
      this.chkNumLock.AutoSize = true;
      this.chkNumLock.Location = new System.Drawing.Point(12, 12);
      this.chkNumLock.Name = "chkNumLock";
      this.chkNumLock.Size = new System.Drawing.Size(86, 17);
      this.chkNumLock.TabIndex = 1;
      this.chkNumLock.Text = "Numeric pad";
      this.chkNumLock.UseVisualStyleBackColor = true;
      // 
      // chkScroll
      // 
      this.chkScroll.AutoCheck = false;
      this.chkScroll.AutoSize = true;
      this.chkScroll.Location = new System.Drawing.Point(12, 58);
      this.chkScroll.Name = "chkScroll";
      this.chkScroll.Size = new System.Drawing.Size(52, 17);
      this.chkScroll.TabIndex = 2;
      this.chkScroll.Text = "Scroll";
      this.chkScroll.UseVisualStyleBackColor = true;
      // 
      // timer
      // 
      this.timer.Enabled = true;
      this.timer.Tick += new System.EventHandler(this.timer_Tick);
      // 
      // notifyIcon
      // 
      this.notifyIcon.ContextMenuStrip = this.contextMenu;
      this.notifyIcon.Icon = this.Icon;
      this.notifyIcon.Text = "KeyboardStatus 1.0";
      this.notifyIcon.Visible = true;
      // 
      // contextMenu
      // 
      this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemShowDialog,
            this.menuItemHideDialog,
            this.menuItemSeparator1,
            this.settingsToolStripMenuItem,
            this.menuItemAbout,
            this.menuItemSeparator2,
            this.menuItemExit});
      this.contextMenu.Name = "contextMenuStrip1";
      this.contextMenu.Size = new System.Drawing.Size(153, 148);
      // 
      // menuItemShowDialog
      // 
      this.menuItemShowDialog.Name = "menuItemShowDialog";
      this.menuItemShowDialog.Size = new System.Drawing.Size(152, 22);
      this.menuItemShowDialog.Text = "Show Dialog";
      this.menuItemShowDialog.Click += new System.EventHandler(this.menuItemShowDialog_Click);
      // 
      // menuItemHideDialog
      // 
      this.menuItemHideDialog.Name = "menuItemHideDialog";
      this.menuItemHideDialog.Size = new System.Drawing.Size(152, 22);
      this.menuItemHideDialog.Text = "Hide Dialog";
      this.menuItemHideDialog.Visible = false;
      this.menuItemHideDialog.Click += new System.EventHandler(this.menuItemHideDialog_Click);
      // 
      // menuItemSeparator1
      // 
      this.menuItemSeparator1.Name = "menuItemSeparator1";
      this.menuItemSeparator1.Size = new System.Drawing.Size(149, 6);
      // 
      // settingsToolStripMenuItem
      // 
      this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
      this.settingsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.settingsToolStripMenuItem.Text = "Settings";
      this.settingsToolStripMenuItem.Visible = false;
      this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
      // 
      // menuItemAbout
      // 
      this.menuItemAbout.Name = "menuItemAbout";
      this.menuItemAbout.Size = new System.Drawing.Size(152, 22);
      this.menuItemAbout.Text = "About";
      this.menuItemAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
      // 
      // menuItemSeparator2
      // 
      this.menuItemSeparator2.Name = "menuItemSeparator2";
      this.menuItemSeparator2.Size = new System.Drawing.Size(149, 6);
      // 
      // menuItemExit
      // 
      this.menuItemExit.Name = "menuItemExit";
      this.menuItemExit.Size = new System.Drawing.Size(152, 22);
      this.menuItemExit.Text = "Exit";
      this.menuItemExit.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
      // 
      // notifyIconNumLock
      // 
      this.notifyIconNumLock.Text = "Numeric pad activated";
      // 
      // notifyIconCapsLock
      // 
      this.notifyIconCapsLock.Text = "Capital letters activated";
      // 
      // notifyIconScroll
      // 
      this.notifyIconScroll.Text = "Scrolling aktivated";
      // 
      // FormStatus
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(108, 87);
      this.ControlBox = false;
      this.Controls.Add(this.chkNumLock);
      this.Controls.Add(this.chkScroll);
      this.Controls.Add(this.chkCapsLock);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "FormStatus";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Keystates";
      this.contextMenu.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.CheckBox chkCapsLock;
    private System.Windows.Forms.CheckBox chkNumLock;
    private System.Windows.Forms.CheckBox chkScroll;
    private System.Windows.Forms.Timer timer;
    private System.Windows.Forms.NotifyIcon notifyIcon;
    private System.Windows.Forms.ContextMenuStrip contextMenu;
    private System.Windows.Forms.ToolStripMenuItem menuItemShowDialog;
    private System.Windows.Forms.ToolStripSeparator menuItemSeparator1;
    private System.Windows.Forms.ToolStripMenuItem menuItemExit;
    private System.Windows.Forms.ToolStripMenuItem menuItemHideDialog;
    private System.Windows.Forms.ToolStripMenuItem menuItemAbout;
    private System.Windows.Forms.ToolStripSeparator menuItemSeparator2;
    private System.Windows.Forms.NotifyIcon notifyIconNumLock;
    private System.Windows.Forms.NotifyIcon notifyIconCapsLock;
    private System.Windows.Forms.NotifyIcon notifyIconScroll;
    private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
  }
}