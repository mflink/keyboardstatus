﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace KeyboardStatus {
  static class Program {
    /// <summary>
    /// Der Haupteinstiegspunkt für die Anwendung.
    /// </summary>
    [STAThread]
    static void Main() {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      FormStatus formStatus = new FormStatus();

      Application.Run();
    }
  }
}