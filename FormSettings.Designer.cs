﻿namespace KeyboardStatus {
  partial class FormSettings {
    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing) {
      this.Hide();
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent() {
      this.groupBoxIcons = new System.Windows.Forms.GroupBox();
      this.radioButtonIconsMulti = new System.Windows.Forms.RadioButton();
      this.radioButtonIconsSingle = new System.Windows.Forms.RadioButton();
      this.panelIconsSingle = new System.Windows.Forms.Panel();
      this.pictureBoxIconsSingle2_numlock = new System.Windows.Forms.PictureBox();
      this.radioButtonIconsSingle2 = new System.Windows.Forms.RadioButton();
      this.pictureBoxIconsSingle1_numlock = new System.Windows.Forms.PictureBox();
      this.radioButtonIconsSingle1 = new System.Windows.Forms.RadioButton();
      this.panelIconsMulti = new System.Windows.Forms.Panel();
      this.radioButtonIconsMulti1 = new System.Windows.Forms.RadioButton();
      this.pictureBoxIconsMulti1 = new System.Windows.Forms.PictureBox();
      this.pictureBoxIconsSingle1_capslock = new System.Windows.Forms.PictureBox();
      this.pictureBoxIconsSingle1_scroll = new System.Windows.Forms.PictureBox();
      this.pictureBoxIconsSingle2_capslock = new System.Windows.Forms.PictureBox();
      this.pictureBoxIconsSingle2_scroll = new System.Windows.Forms.PictureBox();
      this.groupBoxIcons.SuspendLayout();
      this.panelIconsSingle.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle2_numlock)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle1_numlock)).BeginInit();
      this.panelIconsMulti.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsMulti1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle1_capslock)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle1_scroll)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle2_capslock)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle2_scroll)).BeginInit();
      this.SuspendLayout();
      // 
      // groupBoxIcons
      // 
      this.groupBoxIcons.Controls.Add(this.panelIconsMulti);
      this.groupBoxIcons.Controls.Add(this.panelIconsSingle);
      this.groupBoxIcons.Controls.Add(this.radioButtonIconsSingle);
      this.groupBoxIcons.Controls.Add(this.radioButtonIconsMulti);
      this.groupBoxIcons.Location = new System.Drawing.Point(12, 12);
      this.groupBoxIcons.Name = "groupBoxIcons";
      this.groupBoxIcons.Size = new System.Drawing.Size(178, 157);
      this.groupBoxIcons.TabIndex = 0;
      this.groupBoxIcons.TabStop = false;
      this.groupBoxIcons.Text = "Icons";
      // 
      // radioButtonIconsMulti
      // 
      this.radioButtonIconsMulti.AutoSize = true;
      this.radioButtonIconsMulti.Checked = true;
      this.radioButtonIconsMulti.Location = new System.Drawing.Point(6, 19);
      this.radioButtonIconsMulti.Name = "radioButtonIconsMulti";
      this.radioButtonIconsMulti.Size = new System.Drawing.Size(121, 17);
      this.radioButtonIconsMulti.TabIndex = 0;
      this.radioButtonIconsMulti.TabStop = true;
      this.radioButtonIconsMulti.Text = "One icon for all keys";
      this.radioButtonIconsMulti.UseVisualStyleBackColor = true;
      this.radioButtonIconsMulti.CheckedChanged += new System.EventHandler(this.radioButtonIconsMulti_CheckedChanged);
      // 
      // radioButtonIconsSingle
      // 
      this.radioButtonIconsSingle.AutoSize = true;
      this.radioButtonIconsSingle.Location = new System.Drawing.Point(6, 73);
      this.radioButtonIconsSingle.Name = "radioButtonIconsSingle";
      this.radioButtonIconsSingle.Size = new System.Drawing.Size(139, 17);
      this.radioButtonIconsSingle.TabIndex = 2;
      this.radioButtonIconsSingle.Text = "Single icon for each key";
      this.radioButtonIconsSingle.UseVisualStyleBackColor = true;
      this.radioButtonIconsSingle.CheckedChanged += new System.EventHandler(this.radioButtonIconsSingle_CheckedChanged);
      // 
      // panelIconsSingle
      // 
      this.panelIconsSingle.Controls.Add(this.pictureBoxIconsSingle2_scroll);
      this.panelIconsSingle.Controls.Add(this.pictureBoxIconsSingle2_capslock);
      this.panelIconsSingle.Controls.Add(this.pictureBoxIconsSingle1_scroll);
      this.panelIconsSingle.Controls.Add(this.pictureBoxIconsSingle1_capslock);
      this.panelIconsSingle.Controls.Add(this.pictureBoxIconsSingle2_numlock);
      this.panelIconsSingle.Controls.Add(this.radioButtonIconsSingle2);
      this.panelIconsSingle.Controls.Add(this.pictureBoxIconsSingle1_numlock);
      this.panelIconsSingle.Controls.Add(this.radioButtonIconsSingle1);
      this.panelIconsSingle.Location = new System.Drawing.Point(25, 96);
      this.panelIconsSingle.Name = "panelIconsSingle";
      this.panelIconsSingle.Size = new System.Drawing.Size(145, 48);
      this.panelIconsSingle.TabIndex = 6;
      // 
      // pictureBoxIconsSingle2_numlock
      // 
      this.pictureBoxIconsSingle2_numlock.Location = new System.Drawing.Point(78, 27);
      this.pictureBoxIconsSingle2_numlock.Name = "pictureBoxIconsSingle2_numlock";
      this.pictureBoxIconsSingle2_numlock.Size = new System.Drawing.Size(16, 16);
      this.pictureBoxIconsSingle2_numlock.TabIndex = 9;
      this.pictureBoxIconsSingle2_numlock.TabStop = false;
      // 
      // radioButtonIconsSingle2
      // 
      this.radioButtonIconsSingle2.AutoSize = true;
      this.radioButtonIconsSingle2.Location = new System.Drawing.Point(3, 26);
      this.radioButtonIconsSingle2.Name = "radioButtonIconsSingle2";
      this.radioButtonIconsSingle2.Size = new System.Drawing.Size(69, 17);
      this.radioButtonIconsSingle2.TabIndex = 8;
      this.radioButtonIconsSingle2.Tag = "2";
      this.radioButtonIconsSingle2.Text = "Version 2";
      this.radioButtonIconsSingle2.UseVisualStyleBackColor = true;
      // 
      // pictureBoxIconsSingle1_numlock
      // 
      this.pictureBoxIconsSingle1_numlock.Location = new System.Drawing.Point(78, 4);
      this.pictureBoxIconsSingle1_numlock.Name = "pictureBoxIconsSingle1_numlock";
      this.pictureBoxIconsSingle1_numlock.Size = new System.Drawing.Size(16, 16);
      this.pictureBoxIconsSingle1_numlock.TabIndex = 6;
      this.pictureBoxIconsSingle1_numlock.TabStop = false;
      // 
      // radioButtonIconsSingle1
      // 
      this.radioButtonIconsSingle1.AutoSize = true;
      this.radioButtonIconsSingle1.Checked = true;
      this.radioButtonIconsSingle1.Location = new System.Drawing.Point(3, 3);
      this.radioButtonIconsSingle1.Name = "radioButtonIconsSingle1";
      this.radioButtonIconsSingle1.Size = new System.Drawing.Size(69, 17);
      this.radioButtonIconsSingle1.TabIndex = 7;
      this.radioButtonIconsSingle1.TabStop = true;
      this.radioButtonIconsSingle1.Tag = "1";
      this.radioButtonIconsSingle1.Text = "Version 1";
      this.radioButtonIconsSingle1.UseVisualStyleBackColor = true;
      // 
      // panelIconsMulti
      // 
      this.panelIconsMulti.Controls.Add(this.pictureBoxIconsMulti1);
      this.panelIconsMulti.Controls.Add(this.radioButtonIconsMulti1);
      this.panelIconsMulti.Location = new System.Drawing.Point(25, 42);
      this.panelIconsMulti.Name = "panelIconsMulti";
      this.panelIconsMulti.Size = new System.Drawing.Size(145, 25);
      this.panelIconsMulti.TabIndex = 7;
      // 
      // radioButtonIconsMulti1
      // 
      this.radioButtonIconsMulti1.AutoSize = true;
      this.radioButtonIconsMulti1.Checked = true;
      this.radioButtonIconsMulti1.Location = new System.Drawing.Point(3, 3);
      this.radioButtonIconsMulti1.Name = "radioButtonIconsMulti1";
      this.radioButtonIconsMulti1.Size = new System.Drawing.Size(69, 17);
      this.radioButtonIconsMulti1.TabIndex = 8;
      this.radioButtonIconsMulti1.TabStop = true;
      this.radioButtonIconsMulti1.Tag = "1";
      this.radioButtonIconsMulti1.Text = "Version 1";
      this.radioButtonIconsMulti1.UseVisualStyleBackColor = true;
      // 
      // pictureBoxIconsMulti1
      // 
      this.pictureBoxIconsMulti1.Location = new System.Drawing.Point(78, 4);
      this.pictureBoxIconsMulti1.Name = "pictureBoxIconsMulti1";
      this.pictureBoxIconsMulti1.Size = new System.Drawing.Size(16, 16);
      this.pictureBoxIconsMulti1.TabIndex = 9;
      this.pictureBoxIconsMulti1.TabStop = false;
      // 
      // pictureBoxIconsSingle1_capslock
      // 
      this.pictureBoxIconsSingle1_capslock.Location = new System.Drawing.Point(100, 4);
      this.pictureBoxIconsSingle1_capslock.Name = "pictureBoxIconsSingle1_capslock";
      this.pictureBoxIconsSingle1_capslock.Size = new System.Drawing.Size(16, 16);
      this.pictureBoxIconsSingle1_capslock.TabIndex = 10;
      this.pictureBoxIconsSingle1_capslock.TabStop = false;
      // 
      // pictureBoxIconsSingle1_scroll
      // 
      this.pictureBoxIconsSingle1_scroll.Location = new System.Drawing.Point(122, 4);
      this.pictureBoxIconsSingle1_scroll.Name = "pictureBoxIconsSingle1_scroll";
      this.pictureBoxIconsSingle1_scroll.Size = new System.Drawing.Size(16, 16);
      this.pictureBoxIconsSingle1_scroll.TabIndex = 11;
      this.pictureBoxIconsSingle1_scroll.TabStop = false;
      // 
      // pictureBoxIconsSingle2_capslock
      // 
      this.pictureBoxIconsSingle2_capslock.Location = new System.Drawing.Point(100, 27);
      this.pictureBoxIconsSingle2_capslock.Name = "pictureBoxIconsSingle2_capslock";
      this.pictureBoxIconsSingle2_capslock.Size = new System.Drawing.Size(16, 16);
      this.pictureBoxIconsSingle2_capslock.TabIndex = 12;
      this.pictureBoxIconsSingle2_capslock.TabStop = false;
      // 
      // pictureBoxIconsSingle2_scroll
      // 
      this.pictureBoxIconsSingle2_scroll.Location = new System.Drawing.Point(122, 27);
      this.pictureBoxIconsSingle2_scroll.Name = "pictureBoxIconsSingle2_scroll";
      this.pictureBoxIconsSingle2_scroll.Size = new System.Drawing.Size(16, 16);
      this.pictureBoxIconsSingle2_scroll.TabIndex = 13;
      this.pictureBoxIconsSingle2_scroll.TabStop = false;
      // 
      // FormSettings
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(203, 182);
      this.Controls.Add(this.groupBoxIcons);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "FormSettings";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Settings";
      this.groupBoxIcons.ResumeLayout(false);
      this.groupBoxIcons.PerformLayout();
      this.panelIconsSingle.ResumeLayout(false);
      this.panelIconsSingle.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle2_numlock)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle1_numlock)).EndInit();
      this.panelIconsMulti.ResumeLayout(false);
      this.panelIconsMulti.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsMulti1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle1_capslock)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle1_scroll)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle2_capslock)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIconsSingle2_scroll)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxIcons;
    private System.Windows.Forms.RadioButton radioButtonIconsMulti;
    private System.Windows.Forms.RadioButton radioButtonIconsSingle;
    private System.Windows.Forms.Panel panelIconsSingle;
    private System.Windows.Forms.PictureBox pictureBoxIconsSingle2_numlock;
    private System.Windows.Forms.RadioButton radioButtonIconsSingle2;
    private System.Windows.Forms.PictureBox pictureBoxIconsSingle1_numlock;
    private System.Windows.Forms.RadioButton radioButtonIconsSingle1;
    private System.Windows.Forms.Panel panelIconsMulti;
    private System.Windows.Forms.PictureBox pictureBoxIconsMulti1;
    private System.Windows.Forms.RadioButton radioButtonIconsMulti1;
    private System.Windows.Forms.PictureBox pictureBoxIconsSingle2_scroll;
    private System.Windows.Forms.PictureBox pictureBoxIconsSingle2_capslock;
    private System.Windows.Forms.PictureBox pictureBoxIconsSingle1_scroll;
    private System.Windows.Forms.PictureBox pictureBoxIconsSingle1_capslock;
  }
}