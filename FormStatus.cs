using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing.Imaging;

namespace KeyboardStatus {
  public partial class FormStatus : Form {
    protected FormAbout formAbout;
    protected FormSettings formSettings;
    protected bool usesSingleIcons;
    protected int singleSet;
    protected int multiSet;

    public FormStatus() {
      this.usesSingleIcons = false;
      this.singleSet = 1;
      this.multiSet = 1;

      InitializeComponent();
      this.formAbout = new FormAbout();
      this.formAbout.Visible = false;
      this.AddOwnedForm(this.formAbout);

      this.formSettings = new FormSettings();
      this.formSettings.Visible = false;
      this.AddOwnedForm(this.formSettings);

      this.notifyIcon.Icon = Properties.Resources._111_1;

      if (this.usesSingleIcons) {
        this.notifyIconNumLock.Icon = (Icon) Properties.Resources.ResourceManager.GetObject("numlock_" + this.singleSet.ToString());
        this.notifyIconCapsLock.Icon = (Icon) Properties.Resources.ResourceManager.GetObject("capslock_" + this.singleSet.ToString());
        this.notifyIconScroll.Icon = (Icon) Properties.Resources.ResourceManager.GetObject("scroll_" + this.singleSet.ToString());
      }
    }

    private void timer_Tick(object sender, EventArgs e) {
      this.chkNumLock.Checked = (GetKeyState(Keys.NumLock.GetHashCode()) != 0);
      this.chkCapsLock.Checked = (GetKeyState(Keys.CapsLock.GetHashCode()) != 0);
      this.chkScroll.Checked = (GetKeyState(Keys.Scroll.GetHashCode()) != 0);

      if (this.usesSingleIcons) {
        this.notifyIconNumLock.Visible = this.chkNumLock.Checked;
        this.notifyIconCapsLock.Visible = this.chkCapsLock.Checked;
        this.notifyIconScroll.Visible = this.chkScroll.Checked;
      } else {
        string iconName = "_" + (this.chkNumLock.Checked ? "1" : "0")
          + (this.chkCapsLock.Checked ? "1" : "0")
          + (this.chkScroll.Checked ? "1" : "0")
          + "_" + this.multiSet.ToString();

        this.notifyIconNumLock.Visible = false;
        this.notifyIconCapsLock.Visible = false;
        this.notifyIconScroll.Visible = false;

        this.notifyIcon.Icon = (Icon) Properties.Resources.ResourceManager.GetObject(iconName);
      }
    }

    [DllImport("user32.dll")]
    public static extern short GetKeyState(int vk);

    private void menuItemShowDialog_Click(object sender, EventArgs e) {
      this.Show();
      this.menuItemShowDialog.Visible = false;
      this.menuItemHideDialog.Visible = true;
    }

    private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
      if (this.components != null) {
        components.Dispose();
      }

      base.Dispose(true);
      Application.Exit();
    }

    private void menuItemHideDialog_Click(object sender, EventArgs e) {
      this.Hide();
      this.menuItemShowDialog.Visible = true;
      this.menuItemHideDialog.Visible = false;
    }

    private void menuItemAbout_Click(object sender, EventArgs e) {
      this.formAbout.Show();
    }

    private void settingsToolStripMenuItem_Click(object sender, EventArgs e) {
      this.formSettings.Show();
    }

    public void setSingleSet(int number) {
      this.singleSet = number;
    }

    public int getSingleSet() {
      return this.singleSet;
    }

    public void setMultiSet(int number) {
      this.multiSet = number;
    }

    public int getMultiSet() {
      return this.multiSet;
    }

    public bool getUsesSingleIcons() {
      return this.usesSingleIcons;
    }

    public void setUsesSingleIcons(bool yesNo) {
      this.usesSingleIcons = yesNo;
    }
  }
}