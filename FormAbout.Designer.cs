﻿namespace KeyboardStatus {
  partial class FormAbout {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing) {
      this.Hide();
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent() {
      this.labelAppName = new System.Windows.Forms.Label();
      this.labelAppVersion = new System.Windows.Forms.Label();
      this.labelCopyright = new System.Windows.Forms.Label();
      this.textBoxAppDescription = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // labelAppName
      // 
      this.labelAppName.AutoSize = true;
      this.labelAppName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelAppName.Location = new System.Drawing.Point(46, 9);
      this.labelAppName.Name = "labelAppName";
      this.labelAppName.Size = new System.Drawing.Size(222, 31);
      this.labelAppName.TabIndex = 0;
      this.labelAppName.Text = "KeyboardStatus";
      // 
      // labelAppVersion
      // 
      this.labelAppVersion.AutoSize = true;
      this.labelAppVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelAppVersion.Location = new System.Drawing.Point(117, 40);
      this.labelAppVersion.Name = "labelAppVersion";
      this.labelAppVersion.Size = new System.Drawing.Size(80, 17);
      this.labelAppVersion.TabIndex = 1;
      this.labelAppVersion.Text = "Version 1.0";
      // 
      // labelCopyright
      // 
      this.labelCopyright.AutoSize = true;
      this.labelCopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelCopyright.Location = new System.Drawing.Point(50, 71);
      this.labelCopyright.Name = "labelCopyright";
      this.labelCopyright.Size = new System.Drawing.Size(215, 17);
      this.labelCopyright.TabIndex = 2;
      this.labelCopyright.Text = "Copyright © Marco Link, 07.2007";
      // 
      // textBoxAppDescription
      // 
      this.textBoxAppDescription.BackColor = System.Drawing.SystemColors.Control;
      this.textBoxAppDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.textBoxAppDescription.Enabled = false;
      this.textBoxAppDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxAppDescription.Location = new System.Drawing.Point(18, 124);
      this.textBoxAppDescription.Multiline = true;
      this.textBoxAppDescription.Name = "textBoxAppDescription";
      this.textBoxAppDescription.Size = new System.Drawing.Size(284, 53);
      this.textBoxAppDescription.TabIndex = 3;
      this.textBoxAppDescription.Text = "Shows the keystate of the current keyboard in the traybar.";
      // 
      // FormAbout
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(314, 189);
      this.Controls.Add(this.textBoxAppDescription);
      this.Controls.Add(this.labelCopyright);
      this.Controls.Add(this.labelAppVersion);
      this.Controls.Add(this.labelAppName);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "FormAbout";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "About";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label labelAppName;
    private System.Windows.Forms.Label labelAppVersion;
    private System.Windows.Forms.Label labelCopyright;
    private System.Windows.Forms.TextBox textBoxAppDescription;




  }
}